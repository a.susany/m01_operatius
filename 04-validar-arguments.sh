# @edt ASIX-M01
# 27-02-2023
# Antonio Susany
#Programa que valida que te 2 arguments i  mostra l'edat i el nom introduït

if [ $# -ne 2]
then    
 echo "ERROR: num args incorrecte"
 echo "USAGE: $0 nom edat"
 exit 1
fi

#2) xixa

echo "nom: $1"
echo "edat: $2"
exit 0

