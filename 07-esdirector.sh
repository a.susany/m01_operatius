# @edt ASIX-M01
# 27-0# @edt ASIX-M01
# 27-02-2023
# Antonio Susany
#Programa que ens diu si una ruta es un directori
# ----------------------------------------------

# Validació de input

if [ $# -ne 1 ];
then
	echo "ERROR: nums args incorrecte"
	echo "USAGE: "
	exit 1
fi

# Validacio es un directori?
if [ ! -d $1 ];
then
	echo "ERROR: nums args incorrecte"
	echo "USAGE: ha de ser un directori"
	exit 2
fi

# xixa
dir=$1

ls $dir
exit 0
