#! /bin/bash
# @edt ASIX-M01
# 27-0# @edt ASIX-M01
# 06-03-2023
# Antonio Susany
#Programa exemples for

#6) llistar tots els login numerats

contador=1
llistar=$(cat /etc/passwd | cut -d: -f1)

for login in $llistar

do 
	echo "$contador: $login"
	contador=$((contador +1))
done

exit 0
#6) numera llista d'arguments

contador=1
llistat=$(ls)

for arch in $llistat
do 
	echo "$contador: $arch"
	contador=$((contador+1))
done

exit 0

#6) numera llista d'arguments

contador=1

for arg in $*
do 
	echo "$contador: $arg"
	contador=$((contador+1))
done

exit 0
#5) Iterar i mostrar la llista d'arguments

for arg in "$*
do 
	echo $arg
done


#4) Iterar i mostrar la llista d'arguments

$*

for arg in $*
do 
	echo $arg
done



#1) Iterar per a un conjunt d'elements

for nom in "pere" "marta" "pau" "anna"

do
   echo "$nom"
done


#2) Iterar per as un conjunt d'elements

for nom in "pere marta pau anna"

do
   echo "$nom"
done

#3) Iterar per al valor d'una variable

llistat=$(ls)
for nom in $llistat
do
	echo "$nom"

done

