#! /bin/bash
# @edt ASIX-M01
# 27-0# @edt ASIX-M01
# 27-02-2023
# Antonio Susany
#Programa que suma els numeros que introdueixes

sumador=0

for num in $*

do 	
	sumador=$((sumador+$num))
	
done

echo "$sumador"

exit 0
