#! /bin/bash
# @edt ASIX-M01
# 27-0# @edt ASIX-M01
# 9-03-2023
# Antonio Susany
# prog num1...
# Descripció: exemples bucle while

# 7) Numera i mostra en majuscules l'entrada estandar

num=1
while read -r line 
do
	echo "$num: $line" | tr 'a-z' 'A-Z'
done
exit 0

# 6) Mostrar stdin linia a linia fins token FI
read -r line
while [ "$line" != "FI" ]
do
	echo "$line"
	read -r line
done
exit 0	

# 5) Mostrar enumerada la entrada estandar
num=1

while read -r line
do
		echo $num $line
		((num++))
done
exit 0	

# 4)Processar entrada estandard linia a linia

while read -r line
do
	echo $line
done
exit 0

# 3)Iterar per una llista d'arguments

while [ -n "$1" ]
do
	echo "$1 $# $*"
	shift
done
exit 0

#2) comptador decreixent n(arg) ...0
MIN=0
num=$1
while [ $num -ge $MIN ]
do
	echo $num
	((num--))
done

exit 0

# 1) Mostrar un comptador del 1-10

num=1
MAX=10
while [ $num -le $MAX ]
do
	echo "$num"
	((num++))
done
exit 0
