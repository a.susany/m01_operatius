#! /bin/bash
# @edt ASIX-M01
# 27-0# @edt ASIX-M01
# 16-03-2023
# Antonio Susany
#Sinpisis: prog dir
#Validar que es rep un argument i que es un director i llistar-ne el contingut. Per llistar el contingut amb un simple ls ja n'hi ha prou

#Validar nº arguments

ERR_NARG=1
ERR_ARGS=2

if [ $# -ne 1 ]; then
	echo "ERR: Ha de ser només un argument"
	echo "usage: $0 dir"
	exit $ERR_NARGS
fi

#Validar argument

dir=$1

if ! [ -d $dir ]; then 
	echo "ERR: Ha de ser un director"
	echo "usage: $0 dir"
	exit $ERR_ARGS
fi

#Xixa
llista=$(ls $dir)
for file in $llista
do
	if [ -d $dir/$file ]; then
		echo "$file Es un directori"
	
	elif [ -f $dir/$file ];then 
		echo "$file Es un regular file"
	
	elif [ -h $dir/$file ];then 
		echo "$file Es un symplic link"
	
	else
		echo "$file Es altre"
	fi
done
exit 0
