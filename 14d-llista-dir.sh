#! /bin/bash
# @edt ASIX-M01
# 27-0# @edt ASIX-M01
# 16-03-2023
# Antonio Susany
#Sinpisis: prog dir
#Validar que es rep un argument i que es un director i llistar-ne el contingut. Per llistar el contingut amb un simple ls ja n'hi ha prou

#Validar nº arguments

ERR_NARG=1
ERR_ARGS=2

if ! [ $# -gt 1 ]; then
	echo "ERR: Ha de ser només un argument"
	echo "usage: $0 dir"
	exit $ERR_NARGS
fi

#Xixa


for dir in $*
do
	echo -e "<<<<<<<<<<<<<<<<<<<<<<<*$dir*>>>>>>>>>>>>>>>>>>>>>>"
	if ! [ -d $dir ];then
		echo "$dir:Ha de ser un directori" >> /dev/stderr
	else
		llista=$(ls $dir)

		for file in $llista
		do
			
			if [ -d $dir/$file ]; then

				echo -e "\t$file --> Es un directori" 
	
			elif [ -f $dir/$file ];then 
				echo -e "\t$file --> Es un regular file"
	
			elif [ -h $dir/$file ];then 
				echo -e "\t$file --> Es un symplic link"
	
			else
				echo -e "\t$file --> Es altre"
			fi
		done
	fi
done
exit 0
