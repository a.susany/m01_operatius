#! /bin/bash
#Març 2023
#@edt asix m01
#
# 17-argsfor.sh [ -a -b -c -d ] arg...

opcions=''
arguments=''
myfile=''
num=''

while [ -n "$1" ]
do
	case $1 in
		-[abcd])
			opcions="$opcions $1";;
		-f)
			myfile=$2
			shift;;
		-n) 
			num=$2
			shift;;	
		*) 
			arguments="$arguments $1";;
	esac
	shift
done
echo "opcions: $opcions"
echo "arguments: $arguments"
echo "file: $myfile"
echo "num: $num"
exit 0











##############################
opcions=""
arguments=""

for arg in $*
do
	case $arg in
		"-a"|"-b"|"-c"|"-d")
			opcions="$opcions $arg";;
		*)
			echo "$arguments $arg";;
	esac
done
echo "opcions: $opcions"
echo "arguments: $arguments"
exit 0

