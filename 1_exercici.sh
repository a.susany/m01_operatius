#! /bin/bash
# @edt ASIX-M01
# 12-03-23
# Antonio Susany


#10. Fer un programa que rep com a argument un número indicatiu del número màxim de
#línies a mostrar. El programa processa stdin línia a línia i mostra numerades un
#màxim de num línies.

maxim=$1
num=1
while read -r line
do
	if [ $num -le $maxim ]; then
		echo "$num $line"
		((num++))
	else
		exit 1

	fi
done 
exit 0

#9. Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el
#sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra
#per stderr.

while read -r line
do
	grep "^$line:" /etc/passwd >> /dev/null 
	if [ $? -eq 0 ]; then 
		echo "$line" >> /dev/stdout
	else
		echo "$line" >> /dev/stderr
	fi
done
exit 0


#8) Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema
# (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per
# stderr.

for nom in $*
do
	grep "^$nom:" /etc/passwd > /dev/null
	
	if [ $? -eq 0 ]; then

		echo "$nom"	
	
	else
		echo "$nom" >> /dev/stderr	
	fi

done
exit 0


# 7) Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
# mostra, si no no.

while read -r line 
do 
	echo "$line" | grep -E "..{60}"
done 
exit 0



#6) Fer un programa que rep com a arguments noms de dies de la setmana i mostra
#   quants dies eren laborables i quants festius. Si l’argument no és un dia de la
#   setmana genera un error per stderr.
for dia in $*
do
	case $dia in 
		"Dilluns"|"Dimarts"|"Dimecres"|"Dijous"|"Divendres")
			echo "$dia: És un dia laborable"
			;;
		"Dissabte"|"Diumenge") 
			echo "$dia: És un dia festiu"
			;;
		*) 
			echo "$dia --> ERR: S'ha de introduir un dia de la setmana vàlid"
	esac
done

exit 0



#5)Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters




#4)Fer un programa que rep com a arguments números de més (un o més) i indica per
# a cada mes rebut quants dies té el més.

for mes in $*
do
	case $mes in 
		"4"|"6"|"9"|"11")
			echo "$mes: Tiene 30 dias"
			;;
		"1"|"3"|"5"|"7"|"8"|"10"|"12")
			echo "$mes: Tiene 31 dias"
			;;
		"2")
			echo "$mes: Tiene 28 dias"
			;;	
	esac
done

exit 0


#3) Fer un comptador des de zero fins al valor indicat per l’argument rebut.

num=0
MAX=$1

while [ $MAX -ge $num ]
do
	echo $num
	((num++))
done
exit 0

#2) Mostar els arguments rebuts línia a línia, tot numerànt-los.

num=1

for arg in $*
do

	echo "$num $arg"
	((num++))

done
exit 0

#1) Mostrar l’entrada estàndard numerant línia a línia

num=1

while read -r line
do
		echo $num $line
		((num++))
done
exit 0	

